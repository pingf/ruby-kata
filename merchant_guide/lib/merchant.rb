require_relative 'adder'
require_relative 'pattern_detector'
require_relative 'pattern_processor'
require_relative 'roman_letter_converter'
require_relative 'star_word_converter'
class Merchant
  include Converter
  include Detector
  include Processor

  attr_accessor :sheet

  def initialize
    @sheet = {}
  end

  def process pat
    clazz_name = pat.keys.select do |k|
      k.start_with? 'Pattern'
    end
    clazz = "Processor::#{clazz_name[0]}".split('::').inject(Object) {|o,c| o.const_get c}
    clazz.process(@sheet, pat)
  end

  def learn s
    process Pattern.detect(s)
  end

  def answer s
    pat = Pattern.detect(s)
    ret = process pat
    pattern_name = pat.keys.select { |k| k.end_with? 'Unit' }
    unit = (' Credits' if pattern_name.length > 0) || ''
    return ret.to_s.gsub(/\.0*$/, '')+unit
  end
end