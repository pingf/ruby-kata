require_relative './spec_helper.rb'

describe 'roman number converter' do
  context 'for a single roman number' do
    specify { expect(Converter::RomanLetter.convert('I')).to be(1) }
    specify { expect(Converter::RomanLetter.convert('V')).to be(5) }
    specify { expect(Converter::RomanLetter.convert('X')).to be(10) }
    specify { expect(Converter::RomanLetter.convert('L')).to be(50) }
    specify { expect(Converter::RomanLetter.convert('C')).to be(100) }
    specify { expect(Converter::RomanLetter.convert('D')).to be(500) }
    specify { expect(Converter::RomanLetter.convert('M')).to be(1000) }
  end
  context 'for a roman number' do
    specify { expect(Converter::RomanLetter.convert(['I', 'V', 'X'])).to eq([1, 5, 10]) }
  end
end

