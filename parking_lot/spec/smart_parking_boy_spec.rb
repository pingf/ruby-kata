require_relative 'spec_helper'
describe SmartParkingBoy do
  context 'when the boy is able to find parking lot that is available' do
    let!(:lot1) { ParkingLot.new 'lot1',5 }
    let!(:lot2) { ParkingLot.new 'lot2',3 }
    let!(:lot3) { ParkingLot.new 'lot3',4 }
    let!(:car) { Car.new 'YAM5G31' }
    let(:boy) { SmartParkingBoy.new }
    before(:example) do
      4.times { |i| lot1.add_car(Car.new "test_car_group1_#{i}") }
      1.times { |i| lot2.add_car(Car.new "test_car_group2_#{i}") }
      3.times { |i| lot3.add_car(Car.new "test_car_group3_#{i}") }
      boy.familiar_with lot1,lot2,lot3
    end
    example "should know all the parking status and find the best one" do
      expect(boy.traverse).to match_array([[lot1,5,4],[lot2,3,1],[lot3,4,3]])
      expect(boy.best).to eq(lot2)
    end
    example "should park the car" do
      ticket=boy.park(car)
      expect(ticket.lot_name).to eq('lot2')
    end
    example "should fetch the car" do
      ticket=boy.park(car)
      fetch_car=boy.fetch(ticket)
      expect(fetch_car).to eq(car)
    end
  end
end