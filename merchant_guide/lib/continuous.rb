module Calculator
  class Continuous
    def self.check a
      a.each_cons(4) { |a, b, c, d| return true if a==b && b==c && c==d }
      false
    end
  end
end
