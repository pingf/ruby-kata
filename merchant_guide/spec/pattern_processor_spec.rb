require_relative './spec_helper'

describe 'pattern_processor' do
  context 'pattern 1 process' do
    it 'given some inputs' do
      ctx = {}
      Processor::PatternRegisterNumber.process(ctx, {'star'=>'glob','roman'=>'I'})
      expect(ctx['glob']).to eq("I")
    end
  end
  context 'pattern 2 process' do
    it 'given some inputs' do
      ctx = { 'glob' => 'I' }
      Processor::PatternRegisterUnit.process(ctx, {'star'=>'glob glob Silver','arabic'=>34})
      expect(ctx['Silver']).to eq(17)
    end
  end
  context 'pattern 3 process' do
    it 'given some inputs' do
      ctx = { 'glob' => 'I' }
      ret = Processor::PatternAnswerNumber.process(ctx, {'star'=>'glob glob'})
      expect(ret).to eq(2)
    end
  end
  context 'pattern 4 process' do
    it 'given some inputs' do
      ctx = { 'glob' => 'I', 'Silver' => 17 }
      ret = Processor::PatternAnswerUnit.process(ctx, {'star'=>'glob glob Silver'})
      expect(ret).to eq(34)
    end
  end
end





