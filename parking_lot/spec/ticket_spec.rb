require_relative './spec_helper'
describe Ticket do
  subject (:ticket) { ticket = Ticket.new 'lot', 1,'YAM5G31'}
  context 'when a car is parked, the ticket is generated' do
    example "should contain the cars number" do
      expect(ticket.car_number).to eq('YAM5G31')
    end
    example "should contain the lot name" do
      expect(ticket.lot_name).to eq('lot')
    end
  end
end