module Detector
  class Pattern
    @pattern_list = [/(?<PatternRegisterNumber>)(?<star>\w+)\s+is\s+(?<roman>[IVXLCDM])/,
                     /(?<PatternRegisterUnit>)(?<star>((\w+)\s+)+)is\s+(?<arabic>\d+)\s+Credits/,
                     /(?<PatternAnswerNumber>)how\s+much\s+is(?<star>(\s+(\w+))+)\s+\?/,
                     /(?<PatternAnswerUnit>)how\s+many\s+Credits\s+is(?<star>(\s+(\w+))+)\s+\?/]

    def self.detect s
      @pattern_list.each_with_index do |pt, i|
        m = pt.match s
        return Hash[ m.names.zip( m.captures ) ] if !m.nil?
      end
    end
  end
end




