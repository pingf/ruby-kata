module Converter
    class StarWord
        @@star_map={}
        def self.prepare(k,v)
            @@star_map[k] = v
        end
        def self.convert raw
            return raw.map { |x| @@star_map[x] } if raw.class == Array
            @@star_map[raw]
        end
    end
end




