require_relative 'spec_helper'
describe Driver do
  let!(:lot) { ParkingLot.new 'lot', 5 }
  let!(:car) { Car.new 'YAM5G31' }
  let!(:driver) { Driver.new }
  context 'when lot is not full' do
    subject(:ticket) { driver.park(lot, car) }
    example do
      expect(ticket.lot_name).to eq('lot')
      expect(ticket.lot_id).to eq(1)
      expect(ticket.car_number).to eq('YAM5G31')
    end
  end
  context 'when lot is full of cars' do
    subject(:ticket) { driver.park(lot, car) }
    example do
      5.times { |i| lot.add_car(Car.new("test car #{i}")) }
      expect(ticket).to be_nil
    end
  end
  context 'when the car is in the lot' do
    let!(:ticket) { driver.park(lot, car) }
    subject(:fetched_car) { driver.fetch(lot,ticket) }
    example do
      expect(fetched_car).to eq(car)
    end
  end
end