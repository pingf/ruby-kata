module Calculator
  class Adder
    def self.sum a
      a.inject(:+)
    end
  end
end
