require_relative './spec_helper.rb'

describe 'roman number' do
  context 'for a roman number' do
    it 'should return related arabic number' do
      num = Number::Roman.new 'VVI'
      expect(num.value).to be(11)
    end
  end
end

