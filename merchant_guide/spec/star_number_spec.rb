require_relative './spec_helper.rb'

describe 'star number' do
  context 'for a roman number' do
    it 'should return related arabic number' do
      num = Number::Star.new ' glob glob pish '
      num.prepare('glob', 'I')
      num.prepare('prok', 'V')
      num.prepare('pish', 'X')
      num.prepare('tegj', 'L')
      expect(num.value).to eq('IIX')
    end
  end
end