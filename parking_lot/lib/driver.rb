class Driver
  def park lot, car
    lot.add_car car
  end
  def fetch lot, ticket
    lot.remove_car ticket
  end
end