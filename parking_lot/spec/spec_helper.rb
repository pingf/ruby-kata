abs_path = File.expand_path("#{File.dirname(__FILE__)}")
Dir.chdir(abs_path) do
  Dir.glob('../lib/**/*.rb') do |file|
    require_relative file
  end
end

