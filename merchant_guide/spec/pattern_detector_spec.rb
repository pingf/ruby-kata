require_relative './spec_helper'

describe 'pattern_detector' do
  context 'pattern 1 learn number' do
    specify { expect(Detector::Pattern.detect('glob is I')).to eq({"PatternRegisterNumber"=>"","star"=>"glob","roman"=>"I"}) }
  end
  context 'pattern 2 learn unit' do
    specify { expect(Detector::Pattern.detect('glob glob Silver is 34 Credits')).to eq({"PatternRegisterUnit"=>"","star"=>"glob glob Silver ","arabic"=>"34"}) }
  end
  context 'pattern 3 how much' do
    specify { expect(Detector::Pattern.detect('how much is glob glob ?')).to eq({"PatternAnswerNumber"=>"","star"=>" glob glob"}) }
  end
  context 'pattern 4 how many units' do
    specify { expect(Detector::Pattern.detect('how many Credits is glob glob Silver ?')).to eq({"PatternAnswerUnit"=>"","star"=>" glob glob Silver"}) }
  end
end
