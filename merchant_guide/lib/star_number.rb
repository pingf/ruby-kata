
require_relative 'adder'
require_relative 'subtractor'
require_relative 'pattern_detector'
require_relative 'pattern_processor'
require_relative 'roman_letter_converter'
require_relative 'star_word_converter'
module Number
  class Star
    include Converter


    def initialize s
      @star_map={}
      @nums = s.split(' ')
    end

    def prepare(k,v)
      @star_map[k] = v
    end

    def value
      @star_map.each { |k,v| StarWord.prepare(k,v) }
      StarWord.convert(@nums).join
    end
  end
end