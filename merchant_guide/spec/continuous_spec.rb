require_relative './spec_helper.rb'

describe 'calculator' do
  context 'for a simple number sequence to sum' do
    specify { expect(Calculator::Continuous.check([5,1,1,1])).to be_falsey }
    specify { expect(Calculator::Continuous.check([5,1,1,1,1])).to be_truthy }
    specify { expect(Calculator::Continuous.check([5,1,1,1,1,1])).to be_truthy }
  end
end