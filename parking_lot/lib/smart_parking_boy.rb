class SmartParkingBoy < Driver
  def initialize
    @lots={}
  end
  def familiar_with *args
    args.each do |e|
      @lots[e.name] = e
    end
  end
  def traverse
    @lots.map do |k,v|
      [v, v.cap,v.current]
    end
  end
  def best
    mind = traverse
    return nil if mind.length == 0
    mind.min_by do |v|
      v[2]-v[1]
    end[0]
  end
  def park car
    lot = best
    return nil if lot.nil?
    super lot,car
  end
  def fetch ticket
    lot = @lots.values.select do |lot|
      lot.has_car? ticket
    end
    lot[0].remove_car ticket
  end
end