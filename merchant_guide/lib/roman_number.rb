
require_relative 'adder'
require_relative 'subtractor'
require_relative 'pattern_detector'
require_relative 'pattern_processor'
require_relative 'roman_letter_converter'
require_relative 'star_word_converter'
module Number
  class Roman
    include Converter
    include Calculator
    def initialize s
     @nums = s.split('')
    end

    def value
      arabic_nums = RomanLetter.convert(@nums)
      calc = Calculator.new
      calc.calculate(arabic_nums)
    end
  end
end