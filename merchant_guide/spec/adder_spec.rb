require_relative './spec_helper.rb'

describe 'calculator' do
  context 'for a simple number sequence to sum' do
    specify { expect(Calculator::Adder.sum([100, 50, 10, 5, 1])).to be(166) }
  end
end

