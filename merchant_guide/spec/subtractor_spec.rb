require_relative './spec_helper.rb'

describe 'calculator' do
  context 'for a simple number sequence to subtract' do
    specify { expect(Calculator::Subtractor.sum([100, 10, 50, 1, 5])).to be(22) }
  end
end

