require_relative 'spec_helper'
describe ParkingLot do
  let(:lot) { ParkingLot.new 'lot',5 }
  let(:car) { Car.new('YAM5G31') }
  context 'when the lot is full of cars' do
    example "should not let any new car to park in" do
      5.times { |i| lot.add_car(Car.new("test car #{i}")) }
      expect(lot.add_car(car)).to be_nil
    end
  end
  context 'when the lot is not full' do
    example "it can let car park" do
      ticket = lot.add_car(car)
      expect(ticket.car_number).to eq('YAM5G31')
    end
  end
  context 'when the lot has cars and your car is parked in it' do
    example "should be able to remove it" do
      ticket = lot.add_car(car)
      car_t = lot.remove_car(ticket)
      expect(car_t).to eq(car)
    end
  end
  context 'when your car is parked in it' do
    example "should be able to remove it" do
      ticket = Ticket.new 'lot',1,'YAM5G31'
      car_t = lot.remove_car(ticket)
      expect(car_t).to be_nil
    end
  end
end