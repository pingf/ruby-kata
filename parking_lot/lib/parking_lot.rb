class ParkingLot
  attr_reader :name,:cap,:current
  def initialize name,cap
    @name = name
    @cap = cap
    @current = 0
    @space = {}
  end
  def add_car car
    return nil if full?
    @space[car.car_number]=car
    @current += 1
    return Ticket.new(@name,@current,car.car_number)
  end
  def remove_car ticket
    return nil if empty? or !has_car?(ticket)
    car = @space[ticket.car_number]
    @space.delete([ticket.car_number])
    @current -= 1
    car
  end
  def has_car? ticket
    @space.has_key?(ticket.car_number)
  end
  def empty?
    @current == 0
  end
  def full?
    @current >= @cap
  end
  def name
    @name
  end
end