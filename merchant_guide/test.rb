require_relative './lib/roman_letter_converter.rb'
require_relative './lib/star_word_converter.rb'
require_relative './lib/pattern_detector.rb'

class Test
    include Converter
    def test
        p RomanLetter.convert('M')
    end
    def test2
        StarWord.prepare('glob','I')
        p StarWord.convert('glob')
    end

    include Detector
    def test3
        Pattern.detect('glob is I')
    end
end

t=Test.new
t.test
t.test2
t.test3
