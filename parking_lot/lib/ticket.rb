class Ticket
  attr_reader :lot_name, :lot_id, :car_number
  def initialize lot_name, lot_id, car_number
    @lot_name = lot_name
    @lot_id = lot_id
    @car_number = car_number
  end
end