require_relative './star_word_converter'
require_relative './roman_letter_converter'
require_relative './adder'
require_relative './subtractor'
module Processor
  class PatternRegisterNumber
    def self.process ctx, data
      ctx[data['star']] = data['roman']
    end
  end
  class PatternRegisterUnit
    def self.process ctx, data
      star = Number::Star.new data['star'].gsub(/\s+\w+\s*$/,'')
      unit = data['star'].split(' ')[-1]
      ctx.each { |k, v| star.prepare(k, v) }
      roman = Number::Roman.new star.value
      ctx[unit]=data['arabic'].to_f/roman.value
    end
  end
  class PatternAnswerNumber
    def self.process ctx, data
      star = Number::Star.new data['star']
      ctx.each { |k, v| star.prepare(k, v) }
      roman = Number::Roman.new star.value
      roman.value
    end
  end
  class PatternAnswerUnit
    def self.process ctx, data
      star = Number::Star.new data['star'].gsub(/\s+\w+\s*$/,'')
      unit = data['star'].split(' ')[-1]
      ctx.each { |k, v| star.prepare(k, v) }
      roman = Number::Roman.new star.value
      roman.value * ctx[unit]
    end
  end
end

