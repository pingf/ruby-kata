require_relative './spec_helper.rb'

describe 'star number converter' do
  before(:example) do
    Converter::StarWord.prepare('glob', 'I')
    Converter::StarWord.prepare('prok', 'V')
    Converter::StarWord.prepare('pish', 'X')
    Converter::StarWord.prepare('tegj', 'L')
  end
  context 'for a single star number' do
    specify { expect(Converter::StarWord.convert('glob')).to eq('I') }
  end
  context 'for a roman number' do
    specify { expect(Converter::StarWord.convert(['glob', 'prok', 'pish', 'tegj'])).to eq(['I','V','X','L']) }
  end
end

