require_relative './spec_helper.rb'

describe 'calculator' do
  context 'for a simple number sequence to sum' do
    it 'calculate the sequence' do
      calc = Calculator::Calculator.new
      expect(calc.calculate([100, 50, 10, 5, 1])).to be(166)
      expect(calc.calculate([100, 50, 10, 50, 1])).to be(191)
    end
  end
end