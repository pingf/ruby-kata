module Calculator
  class Subtractor
    def self.sum a
      sum = 0
      a.each_cons(2) { |l, r| sum+=l*2 if rule_check(l, r) }
      sum
    end

    private
    def self.rule_check(l, r)
      return (rule0(l, r) and (rule1(l, r) or rule2(l, r) or rule3(l, r)))
    end

    def self.rule0(l, r)
      l<r
    end

    def self.rule1(l, r)
      l==1 if r==5 or r==10
    end

    def self.rule2(l, r)
      l==10 if r==50 or r==100
    end

    def self.rule3(l, r)
      l==100 if r==500 or r==1000
    end
  end
end
