require_relative './spec_helper'

describe 'merchant' do
  context 'for simple test' do
    it 'should be smart enough for processing simple data' do
      m = Merchant.new
      m.learn('glob is I')
      expect(m.sheet['glob']).to eq('I')
      m.learn('glob glob Silver is 34 Credits')
      expect(m.sheet['Silver']).to eq(17)
      ans = m.answer('how much is glob glob ?')
      expect(ans).to eq('2')
      ans = m.answer('how many Credits is glob glob Silver ?')
      expect(ans).to eq('34 Credits')
    end
  end
  context 'for complex test' do
    it 'should be smart enough for processing simple data' do
      m = Merchant.new
      m.learn('glob is I')
      m.learn('prok is V')
      m.learn('pish is X')
      m.learn('tegj is L')
      m.learn('glob glob Silver is 34 Credits')
      m.learn('glob prok Gold is 57800 Credits')
      m.learn('pish pish Iron is 3910 Credits')
      ans = m.answer('how much is pish tegj glob glob ?')
      expect(ans).to eq('42')
      ans = m.answer('how many Credits is glob prok Silver ?')
      expect(ans).to eq('68 Credits')
      ans = m.answer('how many Credits is glob prok Gold ?')
      expect(ans).to eq('57800 Credits')
      ans = m.answer('how many Credits is glob prok Iron ?')
      expect(ans).to eq('782 Credits')
    end
  end
end